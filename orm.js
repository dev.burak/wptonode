const Sequelize = require('sequelize') 
require('dotenv').config()

const userModel = require('./model/users')
const optionsModel = require('./model/options')

// create sequelize object with .env variables
const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
      dialect:process.env.dialect,
      host:process.env.DB_HOST,
      port:process.env.DB_PORT,
    }
  ) // Example for connection

  
  const Users = userModel.Users(sequelize);
  const UserMeta = userModel.UserMeta(sequelize);
  const Options = optionsModel.Options(sequelize);

  //relations
  Users.hasMany(UserMeta ,{ 
    onDelete: 'CASCADE',
    constraints: false});
  // UserMeta.belongsTo(Users);
  

  sequelize.sync().then(() => {
    console.log(`Database & tables created!`)
    
  })

  module.exports = { Users,UserMeta,Options  }