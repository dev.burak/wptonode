const bcrypt = require('bcrypt');
const saltRounds = 12;

const salt = bcrypt.genSaltSync(saltRounds);

async function checkUser(password, hashedpassword) {
    //... fetch user from a db etc.
    const match = await bcrypt.compare(password, hashedpassword);
    return match
  
}

async function createHash(text){
     const hash = await bcrypt.hashSync(text, salt);
     return hash;
}

function randomString(length){
    let str = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charsLength = chars.length;
  
    for (let i = 0; i < length; ++i) {
      str += chars[getRandomInt(0, charsLength - 1)];
    }
  
    return str;
  
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
 

module.exports={createHash,checkUser,randomString}