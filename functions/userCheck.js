const db = require('../orm');
const { Op } = require("sequelize");

async function chekEmail(email){
   const user = await db.Users.findOne({ where: { user_email: email} })

   if(user === null) return false
   return true
}

async function authWithToken(role, token) {
    const user = await db.Users.findAll({
        attributes: { exclude: ['user_pass','UserMeta.Id'] },
        where: {
            [Op.and]: [
                { access_token: token }, 
                { token_expire: { [Op.gt]: new Date() }}, 
                {'$UserMeta.meta_key$':'role'},
                {'$UserMeta.meta_value$':role}
            ]
        },include: {
            model: db.UserMeta,
            as:'UserMeta',
            required: true
          }
    })
    return user
}


module.exports={chekEmail,authWithToken}