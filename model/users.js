const { DataTypes } = require('sequelize');
const crypt = require('../functions/hash');

const Users = (sequelize)=>{ 
    return sequelize.define('Users', { 
        Id:{
            type: DataTypes.BIGINT,
            allowNull:false,
            primaryKey: true,
            autoIncrement: true,
        },
        user_login:{
            type:DataTypes.STRING(60),
            allowNull:false,
            unique: true
        },
        user_pass:{
            type:DataTypes.STRING(255),
            allowNull:false,
        },
        user_nicename:{
            type:DataTypes.STRING(50),
            allowNull:true
        },
        user_email:{
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: true,
            validate: {
                isEmail: {
                    msg: "Must be a valid email address",
                }
            }
        },
        user_url:{
            type:DataTypes.STRING,
            allowNull:true
        },
        user_registered:{
            type:DataTypes.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        user_activation_key:{
            type:DataTypes.STRING,
            allowNull:true
        },
        user_status:{
            type:DataTypes.INTEGER,
            defaultValue:0
        },
        display_name:{
            type:DataTypes.STRING,
            allowNull:true
        },
        access_token:{
            type:DataTypes.STRING,
            allowNull:true
        },
        token_expire:{
            type:DataTypes.DATE,
            allowNull:true
        }
    },{
        indexes: [{
          fields: ['user_email'],
          unique: true,
        }]
      })  
}


const UserMeta =  (sequelize)=>{ 
    return sequelize.define('UserMeta', { 
        Id:{
            type: DataTypes.BIGINT,
            allowNull:false,
            primaryKey: true,
            autoIncrement: true,
        },
        meta_key:{
            type:DataTypes.STRING,
            allowNull:false
        },
        meta_value:{
            type:DataTypes.TEXT
        }
    })
}



module.exports = {Users,UserMeta}