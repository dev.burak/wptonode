const { DataTypes } = require('sequelize');

const Options = (sequelize)=>{ 
    return sequelize.define('Options', { 
        Id:{
            type: DataTypes.BIGINT,
            allowNull:false,
            primaryKey: true,
            autoIncrement: true,
        },
        name:{
            type:DataTypes.STRING,
            allowNull:false,
            unique:true
        },
        value:{
            type:DataTypes.TEXT
        },
        autoload:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }

    },{timestamps: false})
}

module.exports={Options}