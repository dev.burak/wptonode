const express = require('express');
const router = express.Router();
const db = require('../orm');
const check = require('../functions/userCheck');


router.post('/meta_values',async (req,res)=>{
    const {items} = req.body
    try {
        const usermeta = await db.UserMeta.bulkCreate(items)
        if (!usermeta) res.status(400).send({ message: 'something worng' })
        res.send(usermeta)
    }
    catch(error){
        console.log(error)
        res.send(error)
    }
   
})

router.post('/meta_value',async (req,res)=>{
    console.log('body' , req.body)
    const {meta_value,meta_key,UserId} = req.body
    const data = req.body
    console.log(data)
   

       db.Users.findByPk(UserId).then(user=>{
           if(!user) return res.status(400).send({ message: 'something worng' })
           db.UserMeta.create(data).then(usermeta=>{
              res.send(usermeta)
           })
       })  
})

router.post('/role', async(req,res)=>{
    const { role} = req
    console.log('role',req.role)
    if(role != 'admin') return res.status(401).send({message:'opps'})
    res.send({message:'admin'})
})


module.exports = router;