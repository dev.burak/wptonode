const express = require('express')
const app = express()

var router = express.Router()

const db = require('../orm');
const { Op } = require("sequelize");

router.use(async function(req, res, next){
    const Authorization = req.get("Authorization") || undefined

    if(!Authorization){  
        req.role= null
       return next('route')
    }
    const token = Authorization.split(' ')[1]
    if(!token){  
        req.role = null
        return next('route')
    }
    const user = await db.Users.findAll({
        attributes: ['UserMeta.meta_value'] ,
        where: {
            [Op.and]: [
                { access_token: token }, 
                { token_expire: { [Op.gt]: new Date() }}, 
                {'$UserMeta.meta_key$':'role'},
            ]
        },include: {
            model: db.UserMeta,
            as:'UserMeta',
            required: true
          }
    })
    // res.send(user)
    req.role = user[0].UserMeta[0].meta_value
    next('route')
  })

  module.exports= router