const express = require('express');
const router = express.Router();
const crypt = require('../functions/hash');
// const { UserMeta } = require('../model/users');
const db = require('../orm');
const { Op } = require("sequelize");
const check = require('../functions/userCheck');
const { hash } = require('bcrypt');

router.post('/login', async (req,res)=>{
    const {username, password} = req.body
    if(!username || !password) return res.status(409).send({message:'missing parameters'})

   const user = await db.Users.findOne(
        {
            where: { [Op.or]: [{ user_email: username }, { user_login: username }] }
        },
        { include: db.UserMeta }
    )

    if(!user) return res.status(403).send({message:'username or password wrong'})

    const checkPass = await crypt.checkUser(password,user.user_pass);

    if(!checkPass) return res.status(403).send({message:'username or password wrong'})

    //access token and expiretime
    const access_token = crypt.randomString(255)
    const option = await db.Options.findOne({ attributes: ['value' ],where:{name:"expiry_time"}})
    console.log(option.value*1 )
    var dt = new Date();
    dt.setHours( dt.getHours() +(option.value*1 ||6) );

   const  updated = await db.Users.update({ access_token: access_token, token_expire: dt },
        {
            where: { Id: user.Id }
        })

    const {user_login,token_expire} = user
    res.send({user_login,token_expire,access_token})
    
});


router.post('/logout', async (req,res)=>{

    try{
    const token = req.get("Authorization").split(' ')[1]
    console.log(token)
    const isAdmin = await check.authWithToken('admin',token)
        console.log(isAdmin)
    if(!token) return res.status(409).send({message:'missing token or parameter'})
    
    const  updated = await db.Users.update({ access_token: null, token_expire: null },
        {
            where: { access_token: token}
        })

        console.log(updated)
        if(updated == null || updated == 0){ 
            return res.status(400).send({message:"something wrong , try again or already logged out "}) 
        }
       return res.send({status:'OK'})
    }
    catch(error){
        res.status(400).send(error)
    }
});


router.post('/createuser',async (req,res)=>{
    const {user_login , user_pass, user_nicename,user_email,display_name,UserMeta,user_url,user_registered,user_activation_key,user_status} = req.body
    const token = req.get("Authorization").split(' ')[1]
    try{
        const isAdmin = check.authWithToken('admin',token)
        console.log(isAdmin)
        const hashedPass = await crypt.createHash(user_pass)

        const user = await db.Users.create({
                user_login:user_login,
                user_pass:hashedPass,
                user_nicename:user_nicename,
                user_email:user_email,
                display_name:display_name,
                user_registered:user_registered,
                user_url:user_url,
                user_status:user_status,
                user_activation_key:user_activation_key,
                UserMeta:UserMeta
          },{ include:db.UserMeta});
    
        if (!user) res.status(400).send({ message: 'User could not be created' })
      
          
          res.send({
              Id:user.Id,
              user:user.user_login,
              email:user.user_email,
              created:true,
              status:'OK'
          })

    }
    catch(errror){
        console.log(errror)
        res.status(400).send({ message: 'User could not be created' })
    }
   
})

//just for developing
router.get('/hash/:text',(req,res)=>{
    const {text} = req.params
    crypt.createHash(text).then(result=>{
        console.log(result)
        res.send({hashText:result, rawtext:text , length:result.length , tlength:text.length})
    })
})

router.get('/random/:length',(req,res)=>{
    console.log(req.role)
    const lgth = req.params.length *1 || 1
    res.send({randomText: crypt.randomString(lgth)})
})


module.exports = router;