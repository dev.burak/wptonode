const express = require('express')
const app = express()
var methodOverride = require('method-override')
require('dotenv').config()
const helmet = require("helmet");
const getHeaderInfo = require('./routes/middleware')

//routes
const auth = require('./routes/auth');
const user= require('./routes/user')
const port = process.env.PORT
app.use(express.json()); //Used to parse JSON bodies
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(methodOverride())
app.use(logErrors)
app.use(clientErrorHandler)
app.use(errorHandler)

app.use(helmet());





app.use('/auth',getHeaderInfo,auth)
app.use('/user',getHeaderInfo,user)




app.get('/', (req, res) => {
  res.send('<p>Hello World!</p>'+
  '<p>check <a href="https://gitlab.com/dev.burak/wptonode"> source on gitlab</a></p>')
})


//Error Handling

function logErrors(err, req, res, next) {
  console.error(err.stack)
  next(err)
}
function clientErrorHandler (err, req, res, next) {
  if (req.xhr) {
    res.status(500).send({ error: 'Something failed!' })
  } else {
    next(err)
  }
}
function errorHandler (err, req, res, next) {
  res.status(500)
  res.render('error', { error: err })
}


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})