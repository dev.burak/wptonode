const Sequelize = require('sequelize') 
require('dotenv').config()

const userModel = require('./model/users')
const optionsModel = require('./model/options')

const crypt = require('./functions/hash');

// create sequelize object with .env variables
const sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
      dialect:process.env.dialect,
      host:process.env.DB_HOST,
      port:process.env.DB_PORT,
    }
  ) // Example for connection

  
  const Users = userModel.Users(sequelize);
  const UserMeta = userModel.UserMeta(sequelize);
  const Options = optionsModel.Options(sequelize);

  //relations
  Users.hasMany(UserMeta ,{ 
    onDelete: 'CASCADE',
    constraints: false});
  // UserMeta.belongsTo(Users);
  

   sequelize.sync({force: true}).then( async () => {
    console.log(`Database & tables created!`)
    const EXPIRYTIME =  process.env.EXPIRYTIME || 6
    const SITENAME = process.env.SITENAME ||"SITE NAME"
    const SITEURL = process.env.SITEURL ||"https://ikon-x.com.tr"
    const CMSURL = process.env.CMSURL ||"https://cms.ikon-x.com.tr"

    Options.bulkCreate([
        {name:"site_name" ,value:SITENAME },
        {name:"site_url" , value:SITEURL},
        {name:"cms_url" , value:CMSURL},
        {name:"expiry_time" , value:EXPIRYTIME}
    ])

    const {USER_NAME,USER_EMAIL,USER_PASSWORD} = process.env

    if(!USER_EMAIL || !USER_NAME || !USER_PASSWORD ){
      console.log("default user could not created")
      return
    }

    try{

      const hashedPass = await crypt.createHash(USER_PASSWORD)

      const user = await Users.create({
        user_login: USER_NAME,
        user_pass: hashedPass,
        user_email: USER_EMAIL,
        UserMeta: [{
          meta_key:"role",
          meta_value:"admin"
        }]
      }, { include: UserMeta });

      if (!user) res.status(400).send({ message: 'User could not be created' })


      console.log(`default user : ${user.user_login} created successfully`)

    }
    catch (errror) {
      console.log(errror)
      res.status(400).send({ message: 'User could not be created' })
    }


  })